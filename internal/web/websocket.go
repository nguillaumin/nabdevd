package web

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"gitlab.com/nguillaumin/nabdevd/internal/protocol"
)

var upgrader = websocket.Upgrader{}

var conn *websocket.Conn

func wsHandler(w http.ResponseWriter, r *http.Request) {
	if conn != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		fmt.Fprintf(w, "There is already a websocket connection (%v). Disconnect it first.\n", conn.RemoteAddr())
		return
	}

	var err error
	conn, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatalf("Error upgrading HTTP connection: %v", err)
	}

	typedPacket := protocol.TypedPacket{}
	for err == nil {
		err = conn.ReadJSON(&typedPacket)
		if err == nil {
			log.Printf("Received message: %v", typedPacket)
		}
	}

	log.Printf("Websocket read error: %v", err)
	conn = nil
}
