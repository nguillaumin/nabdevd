package web

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"net/http"
	"strings"

	"gitlab.com/nguillaumin/nabdevd/internal/protocol"
	"gitlab.com/nguillaumin/nabdevd/internal/service"
)

//go:embed assets
var embeddedFiles embed.FS

type webHandler struct {
	services *[]service.Service
}

// Initialize initializes the web interface
func Initialize(webPort uint, services *[]service.Service) {
	fsys, _ := fs.Sub(embeddedFiles, "assets")
	fs := http.FileServer(http.FS(fsys))
	http.Handle("/", fs)

	webHandler := webHandler{
		services: services,
	}

	http.Handle("/rest/", &webHandler)
	http.HandleFunc("/ws", wsHandler)

	go http.ListenAndServe(fmt.Sprintf(":%v", webPort), nil)
}

func (webHandler *webHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	infos := make([]protocol.Animation, 0)
	if strings.HasSuffix(r.URL.Path, "/info") {
		for _, service := range *webHandler.services {
			for _, animation := range service.Infos {
				infos = append(infos, animation)
			}
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if b, err := json.Marshal(infos); err == nil {
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}
}
