package protocol

import "time"

// PacketMessage represents a message for the rabbit to broadcast
type PacketMessage struct {
	Type        string    `json:"type"`
	Signature   Message   `json:"signature"`
	Body        Message   `json:"body"`
	Expiration  time.Time `json:"expiration"`
	Cancellable bool      `json:"cancellable"`
}

// Message holds sounds to play and choregraphy
type Message struct {
	Audio        []string `json:"audio"`
	Choreography []string `json:"choreography"`
}
