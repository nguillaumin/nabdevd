package protocol

// PacketMode represents a mode change for a service
type PacketMode struct {
	Type   string   `json:"type"`
	Mode   string   `json:"mode"`
	Events []string `json:"events"`
}
