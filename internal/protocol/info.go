package protocol

// PacketInfo represents the visual animation of the rabbit
type PacketInfo struct {
	Type      string     `json:"type"`
	InfoID    string     `json:"info_id"`
	Animation *Animation `json:"animation"`
}

// Animation represent the visual animation sequence
type Animation struct {
	Tempo  int     `json:"tempo"`
	Colors []Color `json:"colors"`
}

// Color represents a list of colors for the rabbit LEDs
type Color struct {
	Left   string `json:"left"`
	Center string `json:"center"`
	Right  string `json:"right"`
}
