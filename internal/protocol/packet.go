package protocol

import (
	"encoding/json"
)

// Packet represents a message packet that can be
// marshalled to JSON
type Packet interface {
	MarshalJson() []byte
}

// TypedPacket represent a packet with a type
type TypedPacket struct {
	Type string `json:"type"`
}

const (
	// PacketTypeState is the type of a 'state' packet
	PacketTypeState = "state"
	// PacketTypeMode is the type of a 'mode' packet
	PacketTypeMode = "mode"
	// PacketTypeInfo is the type of an 'info' packet
	PacketTypeInfo = "info"
	// PacketTypeMessage is the type of a 'message' packet
	PacketTypeMessage = "message"
	// PacketTypeGestalt is the type of a 'gestalt' packet
	PacketTypeGestalt = "gestalt"
)

func (packet TypedPacket) String() string {
	bytes, err := json.MarshalIndent(packet, "", "  ")
	if err != nil {
		return err.Error()
	}

	return string(bytes)
}

// PacketType returns the type of a packet, given a marshalled
// Packet in JSON form
func PacketType(b []byte) (string, error) {
	packet := &TypedPacket{}
	err := json.Unmarshal(b, packet)
	if err != nil {
		return "", err
	}

	return packet.Type, nil
}
