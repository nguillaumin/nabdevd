package protocol

import "encoding/json"

// PacketGestalt represents a gestalt internal packet
type PacketGestalt struct {
	Type      string `json:"type"`
	RequestID string `json:"request_id"`
}

// PacketGestaltResponse represents a response to a gestalt requets
type PacketGestaltResponse struct {
	Type        string   `json:"type"`
	RequestID   string   `json:"request_id"`
	State       string   `json:"state"`
	Uptime      int      `json:"uptime"`
	Connections int      `json:"connections"`
	Hardware    Hardware `json:"hardware"`
}

// Hardware represents the hardware state of the rabbit
type Hardware struct {
	Model          string `json:"model"`
	SoundCard      string `json:"sound_card"`
	SoundInput     bool   `json:"sound_input"`
	LeftEarStatus  string `json:"left_ear_status"`
	RightEarStatus string `json:"right_ear_status"`
	RFID           bool   `json:"rfid"`
}

// MarshallJSON marshals a PacketState into JSON
func (packet PacketGestaltResponse) MarshallJSON() ([]byte, error) {
	return json.Marshal(packet)
}
