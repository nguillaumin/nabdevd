package protocol

import (
	"encoding/json"
)

// PacketState represents the current state of the rabbit
type PacketState struct {
	Type  string `json:"type"`
	State string `json:"state"`
}

// MarshallJSON marshals a PacketState into JSON
func (packet PacketState) MarshallJSON() ([]byte, error) {
	return json.Marshal(packet)
}
