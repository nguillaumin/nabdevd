package service

import (
	"encoding/json"
	"log"
	"net"

	"gitlab.com/nguillaumin/nabdevd/internal/protocol"
)

// Service represents a service connected to nabdevd
type Service struct {
	Conn   net.Conn
	Mode   string
	Events []string
	Infos  map[string]protocol.Animation
}

// HandlePacket handles a packet sent by a service to nabdevd
func (service *Service) HandlePacket(packetType string, b []byte) error {
	var err error

	switch packetType {
	case protocol.PacketTypeMode:
		packet := &protocol.PacketMode{}
		if err = json.Unmarshal(b, packet); err == nil {
			service.HandlePacketMode(*packet)
		}
	case protocol.PacketTypeInfo:
		packet := &protocol.PacketInfo{}
		if err = json.Unmarshal(b, &packet); err == nil {
			service.HandlePacketInfo(*packet)
		}
	case protocol.PacketTypeMessage:
		packet := &protocol.PacketMessage{}
		if err = json.Unmarshal(b, &packet); err == nil {
			service.HandlePacketMessage(*packet)
		}
	case protocol.PacketTypeGestalt:
		packet := &protocol.PacketGestalt{}
		if err = json.Unmarshal(b, &packet); err == nil {
			service.HandlePacketGestalt(*packet)
		}
	}

	return err
}

// HandlePacketMode handles a 'mode' packet
func (service *Service) HandlePacketMode(packet protocol.PacketMode) {
	service.Mode = packet.Mode
	service.Events = packet.Events
}

// HandlePacketInfo handles an 'info' packet
func (service *Service) HandlePacketInfo(packet protocol.PacketInfo) {
	if packet.Animation == nil {
		delete(service.Infos, packet.InfoID)
	} else {
		service.Infos[packet.InfoID] = *packet.Animation
	}
}

// HandlePacketMessage handles a 'message' packet
func (service *Service) HandlePacketMessage(packet protocol.PacketMessage) {
	//TODO: Enqueue message
}

// HandlePacketGestalt handles a 'gestalt' packet
func (service *Service) HandlePacketGestalt(packet protocol.PacketGestalt) {
	response := protocol.PacketGestaltResponse{
		Type:        "response",
		RequestID:   "gestalt",
		State:       "state",
		Uptime:      0,
		Connections: 0,
		Hardware: protocol.Hardware{
			Model:          "nabdevd",
			SoundCard:      "dummy",
			SoundInput:     false,
			LeftEarStatus:  "dummy",
			RightEarStatus: "dummy",
			RFID:           false,
		},
	}

	b, err := response.MarshallJSON()
	if err != nil {
		log.Fatalf("Error marshalling the idle packet to JSON: %v", err)
	}

	service.Conn.Write(b)
	service.Conn.Write([]byte("\r\n"))
}
