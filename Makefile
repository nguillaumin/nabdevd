.PHONY: lint test build clean

all: lint test build

lint:
	golint -set_exit_status ./...
	go vet ./...

test:
	go test -cover ./...

build:
	CGO_ENABLED=0 go build cmd/nabdevd/nabdevd.go

clean:
	go clean
	rm -f nabdevd
