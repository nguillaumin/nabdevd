package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net"

	"gitlab.com/nguillaumin/nabdevd/internal/protocol"
	"gitlab.com/nguillaumin/nabdevd/internal/service"
	"gitlab.com/nguillaumin/nabdevd/internal/web"
)

var services = make([]service.Service, 0)

func main() {
	port := flag.Uint("port", 10543, "Listen port")
	addr := flag.String("address", "", "Listen address. Blank for 0.0.0.0")
	webPort := flag.Uint("webPort", 3000, "Web interface port")

	log.Printf("Starting web interface on %v", *webPort)
	web.Initialize(*webPort, &services)

	listenAddr := fmt.Sprintf("%v:%v", *addr, *port)
	log.Printf("Listening on %v", listenAddr)

	ln, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalf("Error listening on %v: %v", listenAddr, err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("Error accepting connection: %v", err)
			continue
		}
		defer conn.Close()
		log.Print("Accepted new service connection")
		service := service.Service{
			Conn:  conn,
			Infos: make(map[string]protocol.Animation),
		}
		services = append(services, service)
		go handleConnection(service)

	}
}

func handleConnection(service service.Service) {
	packet := protocol.PacketState{
		Type:  "state",
		State: "idle",
	}

	b, err := packet.MarshallJSON()
	if err != nil {
		log.Fatalf("Error marshalling the idle packet to JSON: %v", err)
	}

	service.Conn.Write(b)
	service.Conn.Write([]byte("\r\n"))

	scanner := bufio.NewScanner(service.Conn)
	for scanner.Scan() {
		log.Printf("Received %v from %v", scanner.Text(), service.Conn.RemoteAddr())
		packetType, err := protocol.PacketType(scanner.Bytes())
		if err != nil {
			log.Printf("Error getting packet type for %v: %v", scanner.Text(), err)
			continue
		}
		log.Printf("Received %v packet", packetType)

		err = service.HandlePacket(packetType, scanner.Bytes())
		if err != nil {
			log.Printf("Error handling %v packet: %v", packetType, err)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Printf("Error reading from %v: %v", service.Conn.RemoteAddr(), err)
	}
	log.Printf("Closing service connection")
}
